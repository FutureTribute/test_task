# test_task

Instructions are written for Linux systems.
1. Get repo

- `git clone https://gitlab.com/FutureTribute/test_task.git`
- ...or download archive and extract it
2. `cd test_task` 
3. Create virtual environment and install dependencies
- `python3 -m venv venv`
- `source venv/bin/activate`
- `pip install -r requirements.txt`

... or install dependencies globally without venv
- `pip3 install -r requirements.txt`
4. `cd catalog`
5. Run local server if venv was created 
- `python manage.py runserver`

... if venv wasn't created
- `python3 manage.py runserver`


Now open `http://localhost:8080/` in your browser.
To add, edit or remove courses open `http://localhost:8080/admin` (login: admin, password: adminadmin) and select "Entries" under "Main".