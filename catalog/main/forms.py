from django import forms
import datetime


class SearchForm(forms.Form):
    course_name = forms.CharField(max_length=100, required=False)
    start_date = forms.DateField(required=False, widget=forms.DateInput(attrs={'type': "date"}))
    course_description = forms.CharField(required=False, widget=forms.Textarea())
