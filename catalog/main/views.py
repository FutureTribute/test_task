from django.shortcuts import render, get_object_or_404
from .models import Entry
from .forms import SearchForm


def index(request):
    context = {"entries": Entry.objects.all(), "catalog": True}
    return render(request, 'main/catalog.html', context)


def detail(request, entry_id):
    entry = get_object_or_404(Entry, pk=entry_id)
    context = {"entry": entry}
    return render(request, 'main/detail.html', context)


def search(request):
    context = {"search": True}
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            clean = form.cleaned_data
            entries = Entry.objects.all()
            if clean["course_name"]:
                entries = entries.filter(name__contains=clean["course_name"])
            if clean["start_date"]:
                entries = entries.filter(start_date=clean["start_date"])
            if clean["course_description"]:
                entries = entries.filter(desc=clean["course_description"])
        context["search_done"] = True
        context["entries"] = entries
    else:
        form = SearchForm()
    context["form"] = form
    return render(request, 'main/catalog.html', context)
