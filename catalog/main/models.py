from django.db import models


class Entry(models.Model):
    name = models.CharField(max_length=100, unique=True)
    image = models.ImageField(upload_to="course_images")
    price = models.DecimalField(max_digits=10, decimal_places=2)
    desc = models.TextField("description", blank=True)
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return self.name
